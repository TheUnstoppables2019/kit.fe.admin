import axios from 'axios';
import configuration from '../config/apis';

export const fetchMenu = () => {
  return axios.get(`${configuration.api.kitmanapis}/api/v1/menus`);
};

export const updateM = (data: any, id: string) => {
  return axios.put(`${configuration.api.kitmanapis}/api/v1/menus/${id}`, {
    content: data,
  });
};
