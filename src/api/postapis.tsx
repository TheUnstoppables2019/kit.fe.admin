import axios from 'axios';
import configuration from '../config/apis';

export interface IPost {
  title: string;
  slug: string;
  categories: number[];
  tags: number[];
  content: string;
  author_id: number;
  thumbnail_image: string;
  status: string;
}

export interface IPostCategory {
  name: string;
}

export interface IViewPost {
  id: number;
  title: string;
  slug: string;
  categories: IPostCategory[];
  tags: number[];
  content: string;
  author_id: number;
  thumbnail_image: string;
  status: string;
}

export const storePost = (data: IPost) => {
  return axios.post(`${configuration.api.blogapis}/api/v1/posts`, data);
};

export const editPost = (data: IPost, id: string) => {
  return axios.put(`${configuration.api.blogapis}/api/v1/posts/${id}`, data);
};

export const viewPost = (id: string) => {
  return axios.get(`${configuration.api.blogapis}/api/v1/posts/${id}`);
};

export const deletePost = (id: number) => {
  return axios.delete(`${configuration.api.blogapis}/api/v1/posts/${id}`);
};

export const viewPosts = () => {
  return axios.get(`${configuration.api.blogapis}/api/v1/posts`);
};

export const getCategories: any = () => {
  return axios.get(`${configuration.api.blogapis}/api/v1/categories`);
};

export const storeResoruces = (data: FormData) => {
  return axios.post(`${configuration.api.blogapis}/api/v1/resources`, data);
};
