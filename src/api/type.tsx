import axios from 'axios';
import configuration from '../config/apis';

export interface IPost {
  title: string;
  slug: string;
  categories: number[];
  tags: number[];
  content: string;
  author_id: number;
  thumbnail_image: string;
  status: string;
}

export interface IPostCategory {
  name: string;
}

export interface IViewPost {
  id: number;
  title: string;
  slug: string;
  categories: IPostCategory[];
  tags: number[];
  content: string;
  author_id: number;
  thumbnail_image: string;
  status: string;
}

export const fetchType = () => {
  return axios.get(`${configuration.api.kitmanapis}/api/v1/types`);
};

export const updateType = (data: any, type: string) => {
  return axios.put(`${configuration.api.kitmanapis}/api/v1/types/${type}`, {
    content: data,
  });
};

export const createType = (data: any, type: string) => {
  return axios.post(`${configuration.api.kitmanapis}/api/v1/types/${type}`, {
    content: data,
  });
};

export const viewType = (slug: string) => {
  return axios.get(
    `${configuration.api.kitmanapis}/api/v1/admin/types/${slug}`
  );
};

export const deleteType = (id: number) => {
  return axios.delete(`${configuration.api.blogapis}/api/v1/type/${id}`);
};

export const viewTypes = () => {
  return axios.get(`${configuration.api.blogapis}/api/v1/type`);
};
