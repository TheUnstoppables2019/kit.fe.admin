import axios from 'axios';
import configuration from '../config/apis';

export interface IPost {
  title: string;
  slug: string;
  categories: number[];
  tags: number[];
  content: string;
  author_id: number;
  thumbnail_image: string;
  status: string;
}

export interface IPostCategory {
  name: string;
}

export interface IViewPost {
  id: number;
  title: string;
  slug: string;
  categories: IPostCategory[];
  tags: number[];
  content: string;
  author_id: number;
  thumbnail_image: string;
  status: string;
}

export const fetchUsers = () => {
  return axios.get(`${configuration.api.kitmanapis}/api/v1/users`);
};

export const createUser = (data: any) => {
  return axios.post(`${configuration.api.kitmanapis}/api/v1/users`, data);
};

export const updateUser = (data: any, id: any) => {
  return axios.put(`${configuration.api.kitmanapis}/api/v1/users/${id}`, data);
};

export const deleteUser = (id: any) => {
  return axios.delete(`${configuration.api.kitmanapis}/api/v1/users/${id}`);
};

export const viewUser = (slug: string) => {
  return axios.get(`${configuration.api.kitmanapis}/api/v1/types/${slug}`);
};
