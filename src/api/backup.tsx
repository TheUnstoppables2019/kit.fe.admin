import axios from 'axios';
import configuration from '../config/apis';

export const storeBackup = (data: any) => {
  return axios.post(`${configuration.api.kitmanapis}/api/v1/backup`, {
    content: data,
  });
};

export const getBackup = () => {
  return axios.get(`${configuration.api.kitmanapis}/api/v1/backup`);
};
