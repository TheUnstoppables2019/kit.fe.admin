module.exports = {
  api: {
    kitmanapis: process.env.KITMANYIU_API || 'http://admin.kitmanyiuapis.com',
    blogapis: process.env.BLOG_API || 'http://www.kit.be.blog.local.com',
  },
};

// https://shrouded-depths-33753.herokuapp.com
