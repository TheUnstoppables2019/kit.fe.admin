export enum INPUT_TYPE {
  TEXT = 'text',
  DROP_DOWN = 'dropdown',
  EDITOR = 'editor',
  CHECKBOX = 'checkbox',
  SOMETHING = 'something',
}
