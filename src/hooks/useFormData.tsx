import { useCallback, useEffect, useState } from 'react';
import { EditorState } from 'draft-js';
import { stateFromHTML } from 'draft-js-import-html';
import { INPUT_TYPE } from '../typescript/form.d';
import { isEmpty } from '../utils/helper';

export default function useFormData(fields: any, data: any = {}) {
  const [formData, setFormData] = useState(fields);
  const [loaded, setIsLoaded] = useState(false);

  const getValue = useCallback(
    (type: any, keyName: any, updatedData: any) => {
      switch (type) {
        case INPUT_TYPE.EDITOR:
          const html = isEmpty(data, keyName) ? '' : data[keyName];
          const blocksFromHTML = stateFromHTML(html);
          try {
            return EditorState.createWithContent(blocksFromHTML);
          } catch {
            return EditorState.createEmpty();
          }
        default:
          return data ? data[keyName] : updatedData[keyName].value;
      }
    },
    [data]
  );

  useEffect(() => {
    if (loaded) {
      return;
    }
    const updatedData = JSON.parse(JSON.stringify(formData));
    Object.keys(formData).forEach((keyName: any) => {
      if (!updatedData[keyName].type) {
        throw new Error(`${keyName} missing Type key`);
      }

      updatedData[keyName].value = getValue(
        updatedData[keyName].type,
        keyName,
        updatedData
      );
      updatedData[keyName].name = keyName;
      updatedData[keyName].type = updatedData[keyName].type.toLowerCase();
    });
    setFormData(updatedData);
    setIsLoaded(true);
  }, [loaded, data, formData, getValue]);

  const onChange = (e: any) => {
    if (!e.target.name) {
      throw new Error(`name empty ${e.target.name}`);
    }
    // hack need to fix

    const v = formData?.content?.value;
    const modData = JSON.parse(JSON.stringify(formData));
    modData[e.target.name].value = e.target.value;
    if (modData.content) {
      modData.content.value = v;
    }
    setFormData(modData);
  };

  const updateValue = (value: any, keyName: string) => {
    if (loaded) {
      const v = formData?.content?.value;
      const modData = JSON.parse(JSON.stringify(formData));
      modData[keyName].value = value;
      if (modData.content) {
        if (keyName !== 'content') {
          modData.content.value = v;
        }
      }
      setFormData(modData);
    }
  };

  return [formData, onChange, updateValue];
}
