import axios from 'axios';
import { shallowEqual, useSelector } from 'react-redux';
import { useState } from 'react';
import { createType, viewType } from '../api/type';
import { getBackup, storeBackup } from '../api/backup';

export default function useBackUp(updateMenu: any, saveTypes: any) {
  const [backUpOption, setBackUpOption] = useState(saveTypes.file.value);

  let fileReader: any;
  const types: any = useSelector(
    (state: any) => state.type.types,
    shallowEqual
  );

  const buildString = async () => {
    const getStuff = types
      .map((item: any) => {
        return item.items.map((i: any) => {
          return {
            cat: item.slug,
            type: i.url,
          };
        });
      })
      .flatMap((t: any) => t);

    const typesRequest = getStuff.map((item: any) => {
      return viewType(`${item.cat}/${item.type}`);
    });
    const result = await axios.all(typesRequest);

    const alld = result
      .map((item: any) => {
        return `${JSON.stringify(item.data[item.data.length - 1])}`;
      })
      .join('~');
    return `${JSON.stringify(types)}~${alld}`;
  };

  const decodeString = (str: string) => {
    const splitResult = str.split('~');
    const res = splitResult.map((item: any) => {
      return JSON.parse(item);
    });
    return res;
  };

  const handleFileRead = () => {
    const res = decodeString(fileReader.result);
    restoreSettingsAndTypes(res);
  };

  const restoreSettingsAndTypes = (res: any) => {
    res.forEach((item: any, index: number) => {
      if (index === 0) {
        updateMenu(JSON.stringify(item));
      } else {
        createType(item.content, item.slug);
      }
    });
  };

  const onChangeBackup = (e: any) => {
    setBackUpOption(e.target.value);
  };

  const backup = async () => {
    const selectedOption = saveTypes[backUpOption];
    const str = await buildString();
    switch (selectedOption.value) {
      case saveTypes.file.value:
        const file = new Blob([str], {
          type: 'text/plain',
        });
        const element = document.createElement('a');
        element.href = URL.createObjectURL(file);
        element.download = 'myFile.kitadmin';
        document.body.appendChild(element); // Required for this to work in FireFox
        element.click();
        break;
      case saveTypes.api.value:
        await storeBackup(str);
        break;
      default:
    }
  };

  const restore = async (e: any) => {
    // @ts-ignore
    const selectedOption = saveTypes[backUpOption];
    switch (selectedOption.value) {
      case saveTypes.file.value:
        fileReader = new FileReader();
        fileReader.onloadend = handleFileRead;
        fileReader.readAsText(e.target.files[0]);
        break;
      case saveTypes.api.value:
        const res = await getBackup();
        restoreSettingsAndTypes(
          decodeString(res.data[res.data.length - 1].content)
        );
        break;
      default:
    }
  };
  return [backup, restore, onChangeBackup, backUpOption];
}
