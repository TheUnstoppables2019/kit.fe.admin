import React, { useEffect, useState } from 'react';
import 'brace/mode/json';
import 'brace/theme/github';
import 'jsoneditor-react/es/editor.min.css';
import { isJsonEmpty } from '../../../utils/helper';
import 'ace-builds/src-noconflict/mode-json';
import 'ace-builds/src-noconflict/theme-monokai';

interface ICodeEditorProps {
  data?: any;
  onChange?: any;
  keys?: any;
}

export const JsonMod: React.FC<ICodeEditorProps> = (props) => {
  // @ts-ignore
  const { data, keys } = props;

  // @ts-ignore
  const [editorData, setEditorData] = useState();
  const [formData, setFormData] = useState({ add: '', remove: '' });
  const [loaded, setIsLoaded] = useState(false);

  useEffect(() => {
    if (loaded) {
      return;
    }

    if (!editorData || isJsonEmpty(editorData)) {
      setEditorData(data);
    }
    setIsLoaded(true);
  }, [data, editorData, loaded]);

  if (!editorData || isJsonEmpty(editorData)) {
    return null;
  }

  // @ts-ignore
  const editorDataKeys = keys;

  const addKey = () => {
    const { onChange } = props;
    const { add } = formData;
    const d = JSON.parse(JSON.stringify(editorData));
    let res = d;
    if (Array.isArray(d)) {
      res = d.map((dt: any) => {
        // @ts-ignore
        dt[add] = dt.hasOwnProperty(add) ? dt[add] : '';
        return dt;
      });
    } else {
      d[add] = d.hasOwnProperty(add) ? d[add] : '';
      res = d;
    }

    // @ts-ignore
    setFormData({ add: '' });
    setEditorData(res);
    onChange(res);
  };

  const removeKey = () => {
    const { onChange } = props;
    const { remove } = formData;
    const d = JSON.parse(JSON.stringify(editorData));
    let res = d;
    if (Array.isArray(d)) {
      res = d.map((dt: any) => {
        if (dt.hasOwnProperty(remove)) {
          delete dt[remove];
        }
        return dt;
      });
    } else {
      delete d[remove];
      res = d;
    }
    setEditorData(res);
    onChange(res);
  };

  const onChange = (e: any) => {
    // @ts-ignore
    setFormData({ [e.target.name]: e.target.value });
  };

  const editorKeysBuilder = () => {
    return (
      <>
        <div>
          <input type="text" name="add" value={add} onChange={onChange} />
          <button type="button" onClick={addKey}>
            Add
          </button>
        </div>
        <div>
          <select name="remove" value={remove} onChange={onChange}>
            {editorDataKeys.map((key: any) => {
              return <option key={key}>{key}</option>;
            })}
          </select>
          <button type="button" onClick={removeKey}>
            Remove
          </button>
        </div>
      </>
    );
  };
  // @ts-ignore
  const { add, remove } = formData;
  return <>{keys && editorKeysBuilder()}</>;
};

export default JsonMod;
