import React from 'react';
import './Save.scss';
import { connect } from 'react-redux';
// eslint-disable-next-line import/named
import { createType } from '../../../api/type';
// eslint-disable-next-line import/no-cycle
import { setHasSaved, setLoading } from '../../../store/action/ui';

interface ISaveProps {
  props?: any;
}

export const Save: React.FC<ISaveProps> = (props: any) => {
  // eslint-disable-next-line no-shadow
  const { isSubmitting, match, data, setLoading, setHasSaved } = props;

  const onClick = () => {
    setLoading(true);
    createType(
      JSON.stringify(data),
      `${match.params.category}/${match.params.type}`
    )
      .then(() => {
        setHasSaved(true);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <button type="button" id="save" onClick={onClick} disabled={isSubmitting}>
      {isSubmitting ? 'Submitting...' : 'Save'}
    </button>
  );
};

const mapStateToProps = (state: any) => ({
  isSubmitting: state.ui.isSubmitting,
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    setLoading: (loading: any) => dispatch(setLoading(loading)),
    setHasSaved: (saved: any) => dispatch(setHasSaved(saved)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Save);
