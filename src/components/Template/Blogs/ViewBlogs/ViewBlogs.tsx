import React, { useCallback, useEffect, useState } from 'react';
import { viewType } from '../../../../api/type';
import ModalButton from '../../../CMS/ModalButton/ModalButton';
import EditBlogForm from '../../../Form/Blog/EditBlogForm';
import './ViewBlogs.css';

type ViewBlogsProps = {
  props: any;
};
// check have post or not if not then merge it. (Main Logic)
const merge = (a: any, b: any, p: any) =>
  a.filter((aa: any) => !b.find((bb: any) => aa[p] === bb[p])).concat(b);
const ViewBlogs = (props: any) => {
  const { onChangeData, data, match } = props;
  const [loaded, setIsLoaded] = useState(false);

  const fetchSkillsData = useCallback(async () => {
    if (!loaded) {
      const res: any = await viewType(`skills/${match.params.type}`);
      const skills = JSON.parse(res.data.content);
      const updateData = merge(
        skills,
        JSON.parse(JSON.stringify(data)),
        'name'
      );
      onChangeData(JSON.stringify(updateData));
      setIsLoaded(true);
    }
  }, [data, match, onChangeData, loaded]);

  useEffect(() => {
    fetchSkillsData();
  }, [fetchSkillsData]);

  const formSubmitted = (stringData: string) => {
    onChangeData(stringData);
  };
  if (!data) {
    return null;
  }

  return (
    <div className="viewBlogsPage">
      {data.map((item: any, propsIndex: number) => {
        return (
          <>
            <h1>
              <b>{item.name}</b>
            </h1>
            <table className="width-full text-left blog-table">
              <tr className="header">
                <th>Concepts</th>
                <th>Status</th>
                <th className="text-right">Action</th>
              </tr>
              {item?.items?.map((data: any, index: number) => {
                const copyData = data;
                if (!copyData.url) {
                  copyData.url = data?.title?.toLowerCase().replace(' ', '-');
                }
                if (!data.title) {
                  data.title = data.name;
                }
                return (
                  <tr>
                    <td className="title">{data.title}</td>
                    <td className="status">
                      <p className={!data.content ? 'missing' : ''}>
                        {!data.content && 'Missing'}
                      </p>
                    </td>
                    <td>
                      <ModalButton
                        text="Edit"
                        classes="minWidth80"
                        buttonClasses="mr-10"
                      >
                        <EditBlogForm
                          data={copyData}
                          index={index}
                          item={props.data}
                          parentName={item.name}
                          propsIndex={propsIndex}
                          match={props.match}
                          formSubmitted={formSubmitted}
                        />
                      </ModalButton>
                    </td>
                  </tr>
                );
              })}
            </table>
          </>
        );
      })}
    </div>
  );
};
export default ViewBlogs;
