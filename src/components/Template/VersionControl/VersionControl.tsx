import React from 'react';

interface IVersionControlProps {
  template?: any;
  onChangeData?: any;
  data?: any;
  prop?: any;
  setDataVersion?: any;
}

export const VersionControl: React.FC<IVersionControlProps> = (props: any) => {
  const { setDataVersion, version } = props;
  return (
    <>
      <input
        type="text"
        onChange={(e: any) => {
          setDataVersion(e.target.value);
        }}
        value={version}
      />
    </>
  );
};

export default VersionControl;
