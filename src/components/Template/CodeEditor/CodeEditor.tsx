import React from 'react';
import 'brace/mode/json';
import 'brace/theme/github';
import 'jsoneditor-react/es/editor.min.css';
import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/mode-json';
import 'ace-builds/src-noconflict/theme-monokai';
import './CodeEditor.scss';

interface ICodeEditorProps {
  data?: any;
  onChangeData?: any;
}

export const CodeEditor: React.FC<ICodeEditorProps> = (props) => {
  const { data } = props;

  if (!data) {
    return null;
  }

  const handleChange = (jsonString: any) => {
    const { onChangeData } = props;
    try {
      onChangeData(jsonString);
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <>
      <AceEditor
        value={JSON.stringify(data, null, 2)}
        mode="json"
        theme="monokai"
        name="aceEditor"
        width="100%"
        showPrintMargin
        showGutter
        highlightActiveLine
        onChange={handleChange}
        editorProps={{
          $blockScrolling: false,
        }}
        setOptions={{
          enableBasicAutocompletion: false,
          enableLiveAutocompletion: false,
          enableSnippets: false,
          showLineNumbers: true,
          tabSize: 2,
        }}
      />
    </>
  );
};

export default CodeEditor;
