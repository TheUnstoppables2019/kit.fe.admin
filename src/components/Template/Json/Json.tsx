import React from 'react';
import './Json.css';
import ace from 'brace';
import 'brace/mode/json';
import 'brace/theme/github';
// @ts-ignore
import { JsonEditor as Editor } from 'jsoneditor-react';
import 'jsoneditor-react/es/editor.min.css';
// import {updateType} from "../../../api/type";
import { isJsonEmpty } from '../../../utils/helper';

interface IMainProps {
  props?: any;
}

interface IMainState {}

export const Json: React.FC<IMainProps> = (props) => {
  // @ts-ignore
  const { data } = props;
  const handleChange = (changedData: any) => {
    // @ts-ignore
    // updateType(props.match.params.type, data);
    // dispatch(addType(data))
    props.onDataChange(changedData);
  };

  if (!data || isJsonEmpty(data)) {
    return null;
  }

  return (
    <Editor
      value={data}
      onChange={handleChange}
      ace={ace}
      theme="ace/theme/github"
      className="json-editor"
    />
  );
};

export default Json;
