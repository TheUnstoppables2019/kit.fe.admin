import React from 'react';
import { connect } from 'react-redux';
import { updateMenu } from '../../store/action/menu';
import useBackUp from '../../hooks/useBackUp';

type BackupProps = {
  updateMenu: any;
};

// eslint-disable-next-line no-shadow
export const Backup = ({ updateMenu }: BackupProps) => {
  const saveTypes = {
    file: {
      label: 'File',
      value: 'file',
    },
    api: {
      label: 'API',
      value: 'api',
    },
  };

  const [backup, restore, onChangeBackup, backUpOption] = useBackUp(
    updateMenu,
    saveTypes
  );

  return (
    <div className="">
      <button type="button" onClick={backup}>
        Backup
      </button>
      {backUpOption === saveTypes.file.value ? (
        <input type="file" id="restore" onChange={restore} />
      ) : (
        <input type="button" id="restore" onClick={restore} value="Restore" />
      )}
      <select onChange={onChangeBackup}>
        {Object.keys(saveTypes).map((item: string) => {
          // @ts-ignore
          const { value, label } = saveTypes[item];
          return <option value={value}>{label}</option>;
        })}
      </select>
    </div>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    // explicitly forwarding arguments
    updateMenu: (data: any) => dispatch(updateMenu(data)),
  };
};

export default connect(null, mapDispatchToProps)(Backup);
