import React from 'react';
import './JsonEditor.css';
import ace from 'brace';
import 'brace/mode/json';
import 'brace/theme/github';
// @ts-ignore
import { JsonEditor as Editor } from 'jsoneditor-react';
import 'jsoneditor-react/es/editor.min.css';

interface IJsonEditorProps {
  data?: any;
  handleChange?: any;
}

export const JsonEditor: React.FC<IJsonEditorProps> = ({
  data,
  handleChange,
}) => {
  if (!data) {
    return null;
  }

  return (
    <Editor
      value={data}
      onChange={handleChange}
      ace={ace}
      theme="ace/theme/github"
      className="json-editor"
    />
  );
};

export default JsonEditor;
