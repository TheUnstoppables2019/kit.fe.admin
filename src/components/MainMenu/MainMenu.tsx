import React, { useState } from 'react';
import './MainMenu.css';
import { ColumnsGap, PlusLg, Gear, Person } from 'react-bootstrap-icons';
import { connect, shallowEqual, useSelector } from 'react-redux';
import { Link, match, useHistory } from 'react-router-dom';
// @ts-ignore
import uuid from 'react-uuid';
import { TYPE_ICON_COMPONENT } from '../../store/typeType.d';
import { website } from '../../config/config';
import { setHasSaved } from '../../store/action/ui';

interface MainMenuParams {
  id: string;
}

interface IMainMenuProps {
  match?: match<MainMenuParams>;
}

export const MainMenu: React.FC<IMainMenuProps> = (props: any) => {
  // eslint-disable-next-line no-shadow
  const { setHasSaved, hasSaved } = props;
  const history = useHistory();
  const types: any = useSelector(
    (state: any) => state.type.types,
    shallowEqual
  );
  const [showUnsavedModal, setShowUnsavedModal] = useState(false);
  const [navPage, setNavPage] = useState('');

  const resetAndNavigatePage = () => {
    setShowUnsavedModal(false);
    setHasSaved(true);
    history.push(navPage);
  };

  return (
    <>
      {showUnsavedModal && (
        <div className="save-model">
          <h1>Changes has not been saved</h1>
          <div className="btn-container">
            <button type="button" onClick={resetAndNavigatePage}>
              Yes
            </button>
            <button
              type="button"
              onClick={() => {
                setShowUnsavedModal(false);
              }}
            >
              Cancel
            </button>
          </div>
        </div>
      )}
      <div className="main-menu">
        <div className="header middle">
          <span className="icon">{website.icon}</span>
        </div>
        <div>
          <h2>Navigation</h2>
          <ul>
            <Link to="/dashboard">
              <li className="middle">
                <ColumnsGap />
                <span>Dashboard</span>
              </li>
            </Link>
          </ul>
          <hr />
          {Array.isArray(types) &&
            types.map((item: any) => {
              return (
                <div key={uuid()}>
                  <h2>{item.title}</h2>
                  <ul>
                    {item.items.map((i: any) => {
                      // @ts-ignore
                      const icon = TYPE_ICON_COMPONENT[i.icon];
                      return (
                        <button
                          type="button"
                          key={uuid()}
                          onClick={() => {
                            const page = `/type/${item.slug}/${i.url}`;
                            setNavPage(page);
                            if (!hasSaved) {
                              setShowUnsavedModal(true);
                              return;
                            }
                            history.push(page);
                          }}
                        >
                          <li className="middle">
                            {icon}
                            <span>{i.menuName ? i.menuName : i.name}</span>
                          </li>
                        </button>
                      );
                    })}
                  </ul>
                  <hr />
                </div>
              );
            })}
          <ul>
            <Link to="/add-item">
              <li className="middle">
                <PlusLg />
                <span>Edit Menu</span>
              </li>
            </Link>
            <Link to="/settings">
              <li className="middle">
                <Gear />
                <span>Settings</span>
              </li>
            </Link>
            <Link to="/users">
              <li className="middle">
                <Person />
                <span>Users</span>
              </li>
            </Link>
          </ul>
        </div>
      </div>
    </>
  );
};
const mapStateToProps = (state: any) => ({
  hasSaved: state.ui.hasSaved,
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    setHasSaved: (saved: any) => dispatch(setHasSaved(saved)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);
