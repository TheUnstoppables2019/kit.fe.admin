import React from 'react';
import { stateToHTML } from 'draft-js-export-html';
import Form from '../../CMS/Form/Form';
import { createType } from '../../../api/type';
import { INPUT_TYPE } from '../../../typescript/form.d';

interface IUsersProps {
  formSubmitted?: any;
  closeModal?: any;
  data?: any;
  index?: number;
  item?: any;
  match?: any;
  propsIndex?: number;
  parentName?: string;
}

export const EditBlogForm: React.FC<IUsersProps> = (props: IUsersProps) => {
  const { formSubmitted, closeModal, data, parentName } = props;
  const fields: any = {
    title: {
      title: 'Title',
      type: INPUT_TYPE.TEXT,
      validation: null,
      list: [],
      value: '',
    },
    categories: {
      title: 'Categories',
      type: INPUT_TYPE.DROP_DOWN,
      validation: null,
      list: [],
      value: '',
    },
    tags: {
      title: 'Tags',
      type: INPUT_TYPE.SOMETHING,
      validation: null,
      value: [parentName, data.title],
    },
    url: {
      title: 'URL',
      type: INPUT_TYPE.TEXT,
      validation: null,
      value: '',
    },
    content: {
      title: 'Content',
      type: INPUT_TYPE.EDITOR,
      validation: null,
      value: '',
    },
  };

  const onSubmit = (submitData: any) => {
    const { item, index = -1, match, propsIndex = -1 } = props;
    const copy = JSON.parse(JSON.stringify(item));
    const copyData = JSON.parse(JSON.stringify(submitData));

    copyData.content = stateToHTML(submitData.content.getCurrentContent());
    copy[propsIndex].items[index] = copyData;

    const stringData = JSON.stringify(copy);
    createType(
      stringData,
      `${match.params.category}/${match.params.type}`
    ).then(() => {
      formSubmitted(stringData);
      closeModal();
    });
  };

  return (
    <Form
      fields={fields}
      data={data}
      onSubmitForm={onSubmit}
      title="Edit Post"
    />
  );
};

export default EditBlogForm;
