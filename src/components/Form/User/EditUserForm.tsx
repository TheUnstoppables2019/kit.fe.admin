import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import Form from '../../CMS/Form/Form';
import { updateUser } from '../../../api/user';

interface IUsersProps {
  selectedUser: any;
  formSubmitted?: any;
  closeModal?: any;
}

export const EditUserForm: React.FC<IUsersProps> = (props: IUsersProps) => {
  const { selectedUser, formSubmitted, closeModal } = props;
  const userFields: any = useSelector((state: any) => {
    return state.userFields.fields;
  }, shallowEqual);

  const onSubmit = (data: any) => {
    updateUser(data, selectedUser.id).then(() => {
      formSubmitted(data, selectedUser.id);
      closeModal();
    });
  };

  return (
    <Form
      fields={userFields}
      data={selectedUser}
      onSubmitForm={onSubmit}
      title="Edit User"
    />
  );
};

export default EditUserForm;
