import React from 'react';
import { deleteUser } from '../../../api/user';

interface IUsersProps {
  userId: any;
  formSubmitted?: any;
  closeModal?: any;
}

export const DeleteUserForm: React.FC<IUsersProps> = (props: IUsersProps) => {
  const { userId, closeModal, formSubmitted } = props;

  const confirmDelete = () => {
    deleteUser(userId).then(() => {
      formSubmitted(userId);
      closeModal();
    });
  };

  return (
    <form className="form">
      <h1 className="text-center vertical-center">Are you sure</h1>
      <button type="button" onClick={confirmDelete}>
        Yes
      </button>
      <button type="button" onClick={closeModal}>
        No
      </button>
    </form>
  );
};

export default DeleteUserForm;
