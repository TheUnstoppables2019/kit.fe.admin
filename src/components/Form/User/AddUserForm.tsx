import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import Form from '../../CMS/Form/Form';
import { createUser } from '../../../api/user';

interface IUsersProps {
  formSubmitted?: any;
}

export const AddUserForm: React.FC<IUsersProps> = (props: IUsersProps) => {
  const { formSubmitted } = props;
  const userFields: any = useSelector((state: any) => {
    return state.userFields.fields;
  }, shallowEqual);

  const onSubmit = (data: any) => {
    // do what ever
    createUser(data).then(() => {
      if (formSubmitted) {
        formSubmitted();
      }
    });
  };

  return <Form fields={userFields} onSubmitForm={onSubmit} title="Add User" />;
};

export default AddUserForm;
