import React from 'react';

interface IInputProps {
  value: any;
  onChange?: any;
  type?: any;
  name?: any;
}

export const Input: React.FC<IInputProps> = (props: IInputProps) => {
  const { type, value, name, onChange } = props;
  return <input type={type} value={value} name={name} onChange={onChange} />;
};

export default Input;
