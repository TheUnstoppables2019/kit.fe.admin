import React from 'react';

interface IDropdownProps {
  value: any;
  onChange?: any;
  name?: any;
  list?: any;
}

export const Dropdown: React.FC<IDropdownProps> = ({
  value,
  onChange,
  list,
  name,
}) => {
  return (
    <select value={value} name={name} onChange={onChange}>
      {list.map((item: any) => {
        return (
          <option key={item.value} value={item.value}>
            {item.text}
          </option>
        );
      })}
    </select>
  );
};

export default Dropdown;
