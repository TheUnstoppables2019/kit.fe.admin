import React, { useState } from 'react';
import './Something.css';
import { X } from 'react-bootstrap-icons';

interface IInputProps {
  value: any;
  onChange?: any;
  name?: any;
  updateValue?: any;
}

export const Something: React.FC<IInputProps> = (props: IInputProps) => {
  const { value, name, updateValue } = props;
  const [latestValue, setLatestValue] = useState(value);
  const [addTagValue, setAddTagValue] = useState('');

  const onChangeValue = (e: any) => {
    setAddTagValue(e.target.value);
  };

  const onClickAddTag = () => {
    setLatestValue([...latestValue, addTagValue]);
    setAddTagValue('');
    updateValue([...latestValue, addTagValue], name);
    // if (onChange) {
    //   onChange(e.target.value);
    // }
  };

  const onRemoveTag = (tagName: string) => {
    setLatestValue(latestValue.filter((item: string) => item !== tagName));
    updateValue(
      latestValue.filter((item: string) => item !== tagName),
      name
    );
  };

  return (
    <>
      <div className="flex full-width something">
        {latestValue?.map((item: any) => {
          return (
            <div className="flex tag">
              <span className="tag-name">{item}</span>
              <X className="close" onClick={() => onRemoveTag(item)} />
            </div>
          );
        })}
      </div>
      <div className="flex full-width">
        <input
          type="text"
          value={addTagValue}
          name={name}
          onChange={onChangeValue}
        />
        <button type="button" onClick={onClickAddTag}>
          Add
        </button>
      </div>
    </>
  );
};

export default Something;
