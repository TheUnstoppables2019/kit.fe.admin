import React from 'react';

type CheckBoxProps = {
  checked: boolean;
  name: string;
  label: string;
  value: number;
  onchange: any;
};

const Checkbox = ({ name, label, value, onchange }: CheckBoxProps) => (
  <div>
    <input type="checkbox" name={name} value={value} onChange={onchange} />
    <label htmlFor={name}>{label}</label>
  </div>
);
export default Checkbox;
