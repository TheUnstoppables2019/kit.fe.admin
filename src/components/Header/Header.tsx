import React, { useRef, useState } from 'react';
import './Header.css';
import { BoxArrowRight, Gear, Person, Search } from 'react-bootstrap-icons';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';

import { NavLink } from 'react-router-dom';
import { logout } from '../../store/action/auth';
import useOutsideAlerter from '../../hooks/useOutsideAlerter';

interface IHeaderState {}

export const Header = () => {
  const [menuActive, setMenuActive] = useState(false);
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef, () => {
    setMenuActive(false);
  });
  const dispatch = useDispatch();
  const auth: any = useSelector((state: any) => state.auth, shallowEqual);

  const toggleMenu = () => {
    // @ts-ignore
    setMenuActive(!menuActive);
  };
  const cssClass = menuActive ? 'active' : 'deActive';

  return (
    <div className="header">
      <div className="input-wrapper flex space-between vertical-center">
        <div>
          <Search className="search-icon" />
          <input className="input-default" placeholder="Search..." />
        </div>
        <div className="rightHeader" ref={wrapperRef}>
          <img
            src={auth.userImage}
            alt="profileImg"
            className="profileImg"
            onClick={toggleMenu}
          />
          <div className={`my-settings item ${cssClass}`}>
            <div className="flex space-between vertical-center header">
              <div className="info flex vertical-center">
                <img
                  src={auth.userImage}
                  alt="profileImg"
                  className="profileImg"
                  onClick={toggleMenu}
                />
                <p className="userName">{auth.profileName}</p>
              </div>
              <div>
                <NavLink
                  onClick={() => dispatch(logout)}
                  to="/logout"
                  className="noHover clearPadding"
                >
                  <BoxArrowRight className="white logout" />
                </NavLink>
              </div>
            </div>
            <NavLink onClick={() => dispatch(logout)} to="/profile">
              <Person />
              <span>Profile</span>
            </NavLink>
            <NavLink onClick={() => dispatch(logout)} to="/settings">
              <Gear />
              <span>Settings</span>
            </NavLink>
            <NavLink onClick={() => dispatch(logout)} to="/logout">
              <BoxArrowRight /> <span>Logout</span>
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
