import React from 'react';
import './Main.css';
import 'brace/mode/json';
import 'brace/theme/github';
import 'jsoneditor-react/es/editor.min.css';
import { capitalizeFirstLetter } from '../../utils/helper';

interface IMainProps {
  title?: string;
  children?: any;
  props?: any;
}

export const Main: React.FC<IMainProps> = ({ title, children, props }) => {
  return (
    <div className="main">
      <div className="banner flex space-between">
        <h1>{capitalizeFirstLetter(title)}</h1>
        <div>
          <p>
            {' '}
            <span className="color-purple">Home -</span>{' '}
            {capitalizeFirstLetter(title)}
          </p>
        </div>
      </div>
      <div className="content">{children}</div>
    </div>
  );
};

export default Main;
