import React from 'react';
import './Form.scss';
import { Editor } from 'react-draft-wysiwyg';
import Input from '../../Form/Input/Input';
import Dropdown from '../../Form/Dropdown/Dropdown';
import useFormData from '../../../hooks/useFormData';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { INPUT_TYPE } from '../../../typescript/form.d';
import Something from '../../Form/Something/Something';

interface IModalProps {
  fields: any;
  data?: any;
  onSubmitForm: any;
  title?: string;
}

export const Form: React.FC<IModalProps> = ({
  fields,
  data,
  onSubmitForm,
  title = 'ACCOUNT LOGIN',
}) => {
  const [formData, onChange, updateValue] = useFormData(fields, data);

  const onSubmit = (e: any) => {
    e.preventDefault();
    const dataKeys = Object.keys(formData);
    const outputData = dataKeys.reduce((acc: any, curr: any) => {
      acc[curr] = formData[curr].value;
      return acc;
    }, {});

    onSubmitForm(outputData);
  };

  const renderType = (keyName: string) => {
    const element = {
      [INPUT_TYPE.TEXT]: <Input {...formData[keyName]} onChange={onChange} />,
      [INPUT_TYPE.DROP_DOWN]: (
        <Dropdown {...formData[keyName]} onChange={onChange} />
      ),
      [INPUT_TYPE.EDITOR]: (
        <Editor
          editorState={formData[keyName].value}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
          onEditorStateChange={(editorState) =>
            updateValue(editorState, keyName)
          }
        />
      ),
      [INPUT_TYPE.SOMETHING]: (
        <Something {...formData[keyName]} updateValue={updateValue} />
      ),
    };

    const { type } = formData[keyName];
    // @ts-ignore
    return element[type];
  };

  return (
    <form className="form" onSubmit={onSubmit}>
      <h1>{title}</h1>
      {Object.keys(formData).map((keyName: any) => {
        return (
          <div className="form-group" key={keyName}>
            <label htmlFor={keyName}>{formData[keyName].title}</label>
            {renderType(keyName)}
          </div>
        );
      })}
      <div className="form-group">
        <input type="submit" value="Save" />
      </div>
    </form>
  );
};

export default Form;
