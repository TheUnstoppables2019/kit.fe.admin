import React, { useState } from 'react';
import './ModalButton.scss';
import Modal from '../Modal/Modal';

interface IModalButtonProps {
  children: any;
  text: any;
  classes?: any;
  buttonClasses?: any;
}

// TODO : Fixed so can be high level component
export const ModalButton: React.FC<IModalButtonProps> = ({
  children,
  classes,
  buttonClasses,
  text = 'Action',
}) => {
  const [modal, setModal] = useState(false);

  const toggleModal = () => {
    setModal(!modal);
  };

  return (
    <>
      <button type="button" onClick={toggleModal} className={buttonClasses}>
        {text}
      </button>
      {modal && (
        <Modal closeModel={toggleModal} classes={classes}>
          {React.cloneElement(children, { closeModal: toggleModal })}
        </Modal>
      )}
    </>
  );
};

export default ModalButton;
