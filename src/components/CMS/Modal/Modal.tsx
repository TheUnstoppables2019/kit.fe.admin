import React from 'react';
import './Modal.scss';
import { X } from 'react-bootstrap-icons';

interface IModalProps {
  children: any;
  closeModel: any;
  classes?: any;
}

// TODO : Fixed so can be high level component
export const Modal: React.FC<IModalProps> = ({
  children,
  closeModel,
  classes,
}) => {
  return (
    <div className="modal-bg">
      <div className={`modal ${classes}`}>
        <X className="close" onClick={closeModel} />
        {children}
      </div>
    </div>
  );
};

export default Modal;
