import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import { BrowserRouter } from 'react-router-dom';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import postReducer from './store/reducer/postReducer';
import * as serviceWorker from './serviceWorker';
import App from './App';
import menuReducer from './store/reducer/menuReducer';
import userFieldsReducer from './store/reducer/userFieldsReducer';
import authReducer from './store/reducer/authReducer';
import uiReducer from './store/reducer/uiReducer';

const rootReducer = combineReducers({
  post: postReducer,
  type: menuReducer,
  userFields: userFieldsReducer,
  auth: authReducer,
  ui: uiReducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
