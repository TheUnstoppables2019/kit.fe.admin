import React, { useCallback, useEffect } from 'react';
import 'kitman-library-base/main.css';
import './App.scss';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import HomePage from './pages/HomePage/HomePage';
import TypePage from './pages/TypePage/TypePage';
import DashboardPage from './pages/DashboardPage/DashboardPage';
import UsersPage from './pages/UsersPage/UsersPage';
import SettingsPage from './pages/SettingsPage/SettingsPage';
import EditMenuPage from './pages/EditMenu/EditMenuPage';
import { getMenu, updateMenu } from './store/action/menu';
import LoginPage from './pages/LoginPage/LoginPage';
import { authCheckState } from './store/action/auth';
import ProtectedRoute from './route/ProtectedRoute/ProtectedRoute';
import Logout from './pages/Logout/Logout';

function App(props: any) {
  const { onTryAutoSignup, getMenu } = props;
  const init = useCallback(() => {
    getMenu();
    onTryAutoSignup();
  }, [getMenu, onTryAutoSignup]);

  useEffect(() => {
    init();
  }, [init]);

  return (
    <>
      <BrowserRouter>
        <Switch>
          <ProtectedRoute path="/" exact component={HomePage} />
          <Route path="/dashboard" exact component={DashboardPage} />
          <Route path="/type/:category/:type" component={TypePage} />
          <Route path="/add-item" component={EditMenuPage} />
          <Route path="/settings" component={SettingsPage} />
          <Route path="/users" component={UsersPage} />
          <Route path="/login" component={LoginPage} />
          <Route path="/logout" component={Logout} />
        </Switch>
      </BrowserRouter>
    </>
  );
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    // explicitly forwarding arguments
    updateMenu: (data: any) => dispatch(updateMenu(data)),
    getMenu: () => dispatch(getMenu()),
    onTryAutoSignup: () => dispatch(authCheckState()),
  };
};

export default connect(null, mapDispatchToProps)(App);
