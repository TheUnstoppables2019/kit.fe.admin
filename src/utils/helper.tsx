export function capitalizeFirstLetter(str: string | undefined): string {
  if (!str) {
    return '';
  }
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function capitalizeFirstLetterAndAddSpace(
  str: string | undefined
): string {
  if (!str) {
    return '';
  }
  str = str.replace(/([A-Z])/g, ' $1').trim();
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function isEmpty(obj: any, key: string) {
  return !Object.prototype.hasOwnProperty.call(obj, key);
}

export function isJsonEmpty(obj: any) {
  for (const prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      return false;
    }
  }

  return JSON.stringify(obj) === JSON.stringify({});
}

export function isJson(str: any) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

export function mapErrorMessage(json: any) {
  const errCode = json.response.status;
  switch (errCode) {
    case 422:
      return 'Missing Email or Password';
    case 401:
      return 'Email or Password incorrect';
    default:
      return 'Server Error';
  }
}
