import React from 'react';
import './EditMenuPage.css';
import { connect } from 'react-redux';
import { Header } from '../../components/Header/Header';
import MainMenu from '../../components/MainMenu/MainMenu';
import Main from '../../components/Main/Main';
import JsonEditor from '../../components/JsonEditor/JsonEditor';
import { updateMenu } from '../../store/action/menu';

interface IAddItemProps {
  props: any;
}

export const EditMenuPage: React.FC<IAddItemProps> = (props: any) => {
  const handleChange = (data: any) => {
    props.updateMenu(JSON.stringify(data));
  };

  return (
    <div className="flex align-stretch">
      <MainMenu />
      <div className="width-full">
        <Header />
        <Main title="Add Item">
          <JsonEditor data={props.types} handleChange={handleChange} />
        </Main>
      </div>
    </div>
  );
};

const mapStateToProps = (state: any) => ({ types: state.type.types });

const mapDispatchToProps = (dispatch: any) => {
  return {
    // explicitly forwarding arguments
    updateMenu: (data: any) => dispatch(updateMenu(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMenuPage);
