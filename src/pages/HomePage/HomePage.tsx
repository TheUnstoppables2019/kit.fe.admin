import React from 'react';
import './HomePage.scss';
import { Header } from '../../components/Header/Header';
import MainMenu from '../../components/MainMenu/MainMenu';
import Main from '../../components/Main/Main';

interface IHomeProps {}

interface IHomeState {}

export class HomePage extends React.Component<IHomeProps, IHomeState> {
  render() {
    return (
      <div className="flex align-stretch">
        <MainMenu />
        <div className="width-full">
          <Header />
          <Main>
            <div>Home</div>
          </Main>
        </div>
      </div>
    );
  }
}

export default HomePage;
