import React from 'react';
import './LoginPage.scss';
import { Key, Person } from 'react-bootstrap-icons';
import { connect } from 'react-redux';
import { website } from '../../config/config';
import { login } from '../../store/action/auth';
import useFormData from '../../hooks/useFormData';
import { mapErrorMessage } from '../../utils/helper';

interface ILoginProps {
  login: any;
  isAuth: any;
  history: any;
  error: any;
}

const field = {
  password: {
    title: 'Password',
    type: 'text',
    value: '',
  },
  email: {
    title: 'Email',
    type: 'email',
    value: '',
  },
};

export const LoginPage = (props: ILoginProps) => {
  const [formField, onChange] = useFormData(field);

  const { history, error } = props;

  const login = (e: any) => {
    e.preventDefault();
    props.login(formField.email.value, formField.password.value);
  };

  if (props.isAuth) {
    history.push('/');
    return <div />;
  }

  return (
    <div className="background">
      <div className="login">
        <h1>LOGIN</h1>
        <h2>Welcome to {website.icon}</h2>
        <div>
          <form className="form">
            <div className="form-group">
              <div className="icon">
                <Person />
              </div>
              <label>USERNAME</label>
              <input
                type="text"
                name="email"
                value={formField.email.value}
                onChange={onChange}
              />
            </div>
            <div className="form-group">
              <div className="icon">
                <Key />
              </div>
              <label>PASSWORD</label>
              <input
                type="password"
                name="password"
                value={formField.password.value}
                onChange={onChange}
              />
            </div>
            <button className="full-width" onClick={login}>
              LOGIN
            </button>
            {error && <p className="error-message">{mapErrorMessage(error)}</p>}
            <div className="footer">
              <span>
                Don't have a account ? <span>Register here</span>
              </span>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  loading: state.auth.loading,
  error: state.auth.error,
  isAuth: state.auth.token !== null,
});
const mapDispatchToProps = (dispatch: any) => ({
  login: (email: string, password: string) => dispatch(login(email, password)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
