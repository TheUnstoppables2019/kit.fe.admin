import React from 'react';
import './DashboardPage.scss';
import { Header } from '../../components/Header/Header';
import MainMenu from '../../components/MainMenu/MainMenu';
import Main from '../../components/Main/Main';

interface IDashboardProps {}

export const DashboardPage: React.FC<IDashboardProps> = () => {
  return (
    <div className="flex align-stretch">
      <MainMenu />
      <div className="width-full">
        <Header />
        <Main title="Dashboard">
          <div>dfs</div>
        </Main>
      </div>
    </div>
  );
};

export default DashboardPage;
