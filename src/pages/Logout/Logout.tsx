import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { logout } from '../../store/action/auth';

class Logout extends React.Component {
  componentDidMount() {
    // @ts-ignore
    const { onLogout } = this.props;
    onLogout();
  }

  render() {
    return <Redirect to="/" />;
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  onLogout: () => dispatch(logout()),
});
export default connect(null, mapDispatchToProps)(Logout);
