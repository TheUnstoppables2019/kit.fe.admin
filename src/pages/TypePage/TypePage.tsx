import React, { useEffect, useState } from 'react';
import './TypePage.css';
import { connect, shallowEqual, useSelector } from 'react-redux';
import { Header } from '../../components/Header/Header';
import MainMenu from '../../components/MainMenu/MainMenu';
import Main from '../../components/Main/Main';
import { viewType } from '../../api/type';
import TemplateSingle from './TemplateSingle/TemplateSingle';
import TemplateArray from './TemplateArray/TemplateArray';
import { setHasSaved } from '../../store/action/ui';

interface ITypeProps {
  props: any;
}

export const TypePage: React.FC<ITypeProps> = (props: any) => {
  const [typeData, setTypeData] = useState({});
  const [allData, setAllData] = useState(null);
  const [loading, setIsLoading] = useState({});
  const [version, setVersion] = useState(0);
  // eslint-disable-next-line no-shadow
  const { hasSaved, setHasSaved, match } = props;
  const types: any = useSelector(
    (state: any) => state.type.types,
    shallowEqual
  );
  // @ts-ignore
  useEffect(() => {
    setIsLoading(true);

    async function fetchData() {
      // You can await here
      const data: any = await viewType(
        `${props.match.params.category}/${props.match.params.type}`
      );
      if (!props.match) {
        return;
      }
      if (!data) {
        setTypeData({});
      }
      setTypeData(JSON.parse(data.data[data.data.length - 1].content));
      setVersion(data.data.length - 1);
      setAllData(data.data);
      setIsLoading(false);
    }

    fetchData();
  }, [props.match]);

  const setDataVersion = (ver: number) => {
    if (!ver || !allData) {
      return;
    }

    // @ts-ignore
    if (ver < allData.length && ver > -1) {
      setVersion(ver);
      // @ts-ignore
      setTypeData(JSON.parse(allData[version].content));
    }
  };

  const onChangeData = (data: any) => {
    try {
      const d = JSON.parse(data);
      if (hasSaved) {
        setHasSaved(false);
      }
      setTypeData(d);
    } catch (e) {
      console.error('err', e);
    }
  };

  const getTemplate = () => {
    const currentType: any = null;
    const menuCategory = types.find(
      (i: any) => i.slug === props.match.params.category
    );
    if (!menuCategory) {
      console.log('Cannot find menu category ', currentType);
      throw new Error('Cannot find menu category');
    }
    const menuType = menuCategory.items.find(
      (item: any) => item.url === props.match.params.type
    );
    if (!menuType) {
      console.log('Cannot find type ', currentType);
      throw new Error('Cannot find type');
    }
    return menuType.template;
  };

  if (!types) {
    return <></>;
  }
  const template = getTemplate();
  const isTemplateArray = Array.isArray(template);
  const renderComp = () => {
    return isTemplateArray ? (
      <TemplateArray
        prop={props}
        match={match}
        templates={template}
        onChangeData={onChangeData}
        data={typeData}
        setDataVersion={setDataVersion}
        version={version}
      />
    ) : (
      <TemplateSingle
        prop={props}
        match={match}
        template={template}
        onChangeData={onChangeData}
        data={typeData}
        setDataVersion={setDataVersion}
        version={version}
      />
    );
  };

  const render = () => {
    return loading ? <p>Loading...</p> : renderComp();
  };

  return (
    <div className="flex align-stretch">
      <MainMenu />
      <div className="width-full">
        <Header />
        <Main title={match.params.type} props={props}>
          {render()}
        </Main>
      </div>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  hasSaved: state.ui.hasSaved,
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    setHasSaved: (saved: any) => dispatch(setHasSaved(saved)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TypePage);
