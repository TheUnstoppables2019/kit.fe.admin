import React from 'react';
import { TYPE_TEMPLATE_COMPONENT } from '../../../store/typeType.d';

interface ITemplateSingleProps {
  templates: any;
  onChangeData: any;
  data: any;
  prop: any;
  setDataVersion: any;
  version: number;
  match: any;
}

export const TemplateArray: React.FC<ITemplateSingleProps> = (props: any) => {
  const { templates } = props;

  return templates.map((template: any) => {
    // @ts-ignore
    const component: any = TYPE_TEMPLATE_COMPONENT[template];
    return component && React.cloneElement(component, props);
  });
};

export default TemplateArray;
