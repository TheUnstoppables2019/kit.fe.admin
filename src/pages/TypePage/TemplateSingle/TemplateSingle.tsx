import React from 'react';
import { TYPE_TEMPLATE_COMPONENT } from '../../../store/typeType.d';

interface ITemplateSingleProps {
  template: any;
  onChangeData: any;
  data: any;
  prop: any;
  setDataVersion: any;
  version: number;
  match: any;
}

export const TemplateSingle: React.FC<ITemplateSingleProps> = (props: any) => {
  const { template } = props;

  // @ts-ignore
  const component: any = TYPE_TEMPLATE_COMPONENT[template];

  return component && React.cloneElement(component, props);
};

export default TemplateSingle;
