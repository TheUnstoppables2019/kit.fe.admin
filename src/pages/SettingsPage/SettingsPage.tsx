import React, { useState } from 'react';
import './SettingsPage.css';
import { Header } from '../../components/Header/Header';
import MainMenu from '../../components/MainMenu/MainMenu';
import Main from '../../components/Main/Main';
import CodeEditor from '../../components/Template/CodeEditor/CodeEditor';
import JsonMod from '../../components/Template/JsonMod/JsonMod';
import Backup from '../../components/Backup/Backup';

interface ISettingsProps {
  props: any;
}

interface ISettingsState {}

export const SettingsPage: React.FC<ISettingsProps> = (props: any) => {
  const tempData = {
    userName: {
      title: 'User Name',
      type: 'text',
      value: '',
    },
    password: {
      title: 'Password',
      type: 'text',
      value: '',
    },
    email: {
      title: 'Email',
      type: 'email',
      value: '',
    },
    role: {
      title: 'Role',
      type: 'dropdown',
      value: '',
    },
  };

  const [settingData, setSettingData] = useState(tempData);

  const onChange = (data: any) => {
    setSettingData(data);
  };

  // @ts-ignore
  return (
    <div className="flex align-stretch">
      <MainMenu />
      <div className="width-full">
        <Header />
        <Main title="Settings">
          <Backup />
          <CodeEditor data={settingData} />
          <JsonMod
            data={settingData}
            keys={Object.keys(settingData)}
            onChange={onChange}
          />
        </Main>
      </div>
    </div>
  );
};

export default SettingsPage;
