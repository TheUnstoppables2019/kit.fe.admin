import React, { useEffect, useState } from 'react';
import './UsersPage.css';
// @ts-ignore
import uuid from 'react-uuid';
import { Header } from '../../components/Header/Header';
import MainMenu from '../../components/MainMenu/MainMenu';
import Main from '../../components/Main/Main';
import { capitalizeFirstLetterAndAddSpace } from '../../utils/helper';
import EditUserForm from '../../components/Form/User/EditUserForm';
import ModalButton from '../../components/CMS/ModalButton/ModalButton';
import AddUserForm from '../../components/Form/User/AddUserForm';
import { fetchUsers } from '../../api/user';
import DeleteUserForm from '../../components/Form/User/DeleteUserForm';

interface IUsersProps {
  props: any;
}

export const UsersPage: React.FC<IUsersProps> = () => {
  const [data, setData] = useState<any | null>();
  const [titles, setTitles] = useState<any | null>();

  useEffect(() => {
    const getUser = async () => {
      const users: any = await fetchUsers();
      setData(users.data);
      setTitles(Object.keys(users.data[0]));
    };
    getUser();
  }, []);

  const renderElement = (val: string, key: string) => {
    if (!val || !key) {
      return <></>;
    }

    return key.toLowerCase().includes('img') ? (
      <img src={val} alt={val} />
    ) : (
      <p>{val}</p>
    );
  };

  const formSubmitted = (updateData: any, id: string) => {
    delete updateData.password;

    const updatedData = data.map((item: any) => {
      return item.id === id ? { ...item, ...updateData } : item;
    });
    setData(updatedData);
  };

  return (
    <div className="flex align-stretch">
      <MainMenu />
      <div className="width-full">
        <Header />
        <Main title="Users">
          <div className="text-right">
            <ModalButton text="Add User" classes="minWidth30">
              <AddUserForm />
            </ModalButton>
          </div>
          <table className="user-table">
            <tr>
              {titles &&
                titles.map((title: any) => {
                  return (
                    <th key={uuid()}>
                      {capitalizeFirstLetterAndAddSpace(title)}
                    </th>
                  );
                })}
              <th>Action</th>
            </tr>
            {data &&
              data.map((user: any) => {
                const keys = Object.keys(user);
                return (
                  <tr key={uuid()}>
                    {keys.map((key: any) => {
                      return (
                        <td key={uuid()}>{renderElement(user[key], key)}</td>
                      );
                    })}
                    <td>
                      <ModalButton
                        text="Edit"
                        classes="minWidth30"
                        buttonClasses="mr-10"
                      >
                        <EditUserForm
                          selectedUser={user}
                          formSubmitted={formSubmitted}
                        />
                      </ModalButton>
                      <ModalButton text="Delete">
                        <DeleteUserForm
                          formSubmitted={formSubmitted}
                          userId={user.id}
                        />
                      </ModalButton>
                    </td>
                  </tr>
                );
              })}
          </table>
        </Main>
      </div>
    </div>
  );
};

export default UsersPage;
