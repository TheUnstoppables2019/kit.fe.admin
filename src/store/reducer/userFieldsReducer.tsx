import * as actionTypes from "../action/actionTypes"
import {UserFieldsAction, UserFieldsState} from "../userFieldsType.d";

const initialState: UserFieldsState = {
    fields: {
        "userName": {
            "title": "User Name",
            "type": "text",
            "value": ""
        },
        "password": {
            "title": "Password",
            "type": "text",
            "value": ""
        },
        "email": {
            "title": "Email",
            "type": "text",
            "value": ""
        },
        "role": {
            "title": "Role",
            "type": "dropdown",
            "value": "",
            "list": [
                {value: 'a', text: 'b'},
                {value: 'c', text: 'c'},
                {value: 'd', text: 'd'},
                {value: 'e', text: 'e'},
            ]
        }
    },
}

const userFieldsReducer = (
    state: UserFieldsState = initialState,
    action: UserFieldsAction
): UserFieldsState => {
    switch (action.type) {
        case actionTypes.ADD_USERFIELD:
            return {
                ...state,
                fields: action.t,
            }
        case actionTypes.REMOVE_USERFIELD:
            const updatedFields: any = state.fields.filter(
                (type: { id: any; }) => type.id !== action.t.id
            )
            return {
                ...state,
                fields: updatedFields,
            }
        case actionTypes.UPDATE_USERFIELD:
            return {
                fields: action.t
            }
    }
    return state
}

export default userFieldsReducer
