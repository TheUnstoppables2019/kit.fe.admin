import * as actionTypes from '../action/actionTypes';
import { MenuAction, MenuState } from '../typeType.d';

const menuReducer = (state: any = {}, action: MenuAction): MenuState => {
  switch (action.type) {
    case actionTypes.GET_MENU:
      return {
        ...state,
        types: action.t !== '' ? JSON.parse(action.t) : [],
        id: action.id,
      };
    case actionTypes.UPDATE_MENU:
      return {
        ...state,
        types: action.t !== '' ? JSON.parse(action.t) : [],
      };
    default:
  }
  return state;
};

export default menuReducer;
