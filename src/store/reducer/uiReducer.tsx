import * as actionTypes from '../action/actionTypes';
import { updateObject } from '../../utils/utils';

const initialState = {
  isSubmitting: false,
  hasSaved: true,
};

const setSubmitting = (state: any, action: any) =>
  updateObject(state, { isSubmitting: action.isSubmitting });

const setHasSaved = (state: any, action: any) =>
  updateObject(state, { hasSaved: action.hasSaved });

const uiReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.SET_SUBMITTING:
      return setSubmitting(state, action);
    case actionTypes.SET_HAS_SAVED:
      return setHasSaved(state, action);
    default:
      return state;
  }
};

export default uiReducer;
