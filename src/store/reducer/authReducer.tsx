import * as actionTypes from '../action/actionTypes';
import {updateObject} from '../../utils/utils';

const initialState = {
    token: null,
    userId: null,
    profileName: null,
    error: null,
    loading: false,
    authRedirectPath: '/',
};

const authStart = (state: any, action: any) => updateObject(state, {error: null, loading: true});

const authSuccess = (state: any, action: any) =>
    updateObject(state, {
        token: action.idToken,
        profileName: action.displayName === '' ? 'Kitman Yiu' : action.displayName,
        userId: action.userId,
        error: null,
        loading: false,
        userImage: action.userImage,
    });
const authFail = (state: any, action: any) => updateObject(state, {error: action.error, loading: false});

const authLogout = (state: any, action: any) => updateObject(state, {token: null, userId: null});

const authReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case actionTypes.AUTH_START:
            return authStart(state, action);
        case actionTypes.AUTH_SUCCESS:
            return authSuccess(state, action);
        case actionTypes.AUTH_FAIL:
            return authFail(state, action);
        case actionTypes.AUTH_LOGOUT:
            return authLogout(state, action);
        default:
            return state;
    }
};

export default authReducer;
