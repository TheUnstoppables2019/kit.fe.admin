import * as actionTypes from './actionTypes';
import { DispatchType, UserFieldsAction } from '../userFieldsType.d';

export function addUserFields(type: any) {
  const action: UserFieldsAction = {
    type: actionTypes.ADD_USERFIELD,
    t: type,
  };
  return simulateHttpRequest(action);
}

export function removeUserFields(type: any) {
  const action: UserFieldsAction = {
    type: actionTypes.REMOVE_USERFIELD,
    t: type,
  };
  return simulateHttpRequest(action);
}

export function updateUserFields(type: any) {
  const action: UserFieldsAction = {
    type: actionTypes.UPDATE_USERFIELD,
    t: type,
  };
  return simulateHttpRequest(action);
}

export function simulateHttpRequest(action: UserFieldsAction) {
  return (dispatch: DispatchType) => {
    setTimeout(() => {
      dispatch(action);
    }, 500);
  };
}
