import * as actionTypes from "./actionTypes"
import { DispatchType, getArticleAction} from "../postType.d";


export function getArticle() {
    const action: getArticleAction = {
        type: actionTypes.GET_ARTICLE,
    }

    return simulateHttpRequest(action)
}

// export function addArticle(article: IArticle) {
//     const action: ArticleAction = {
//         actionType: actionTypes.ADD_ARTICLE,
//         article,
//     }
//
//     return simulateHttpRequest(action)
// }
//
// export function removeArticle(article: IArticle) {
//     const action: ArticleAction = {
//         actionType: actionTypes.REMOVE_ARTICLE,
//         article,
//     }
//     return simulateHttpRequest(action)
// }

export function simulateHttpRequest(action: any) {
    return (dispatch: DispatchType) => {
        setTimeout(() => {
            dispatch(action)
        }, 500)
    }
}
