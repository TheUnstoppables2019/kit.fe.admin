import { uiDispatchType } from '../typeType.d';
import * as actionTypes from './actionTypes';

// eslint-disable-next-line import/prefer-default-export
export function setLoading(submitting: any) {
  return async (dispatch: uiDispatchType) => {
    return dispatch({
      type: actionTypes.SET_SUBMITTING,
      isSubmitting: submitting,
    });
  };
}

export function setHasSaved(saved: any) {
  return async (dispatch: uiDispatchType) => {
    return dispatch({
      type: actionTypes.SET_HAS_SAVED,
      hasSaved: saved,
    });
  };
}
