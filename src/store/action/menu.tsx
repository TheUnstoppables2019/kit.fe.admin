import * as actionTypes from './actionTypes';
import { DispatchType } from '../typeType.d';
import { fetchMenu, updateM } from '../../api/menu';

export function getMenu() {
  return async (dispatch: DispatchType) => {
    const res: any = await fetchMenu();
    return dispatch({
      type: actionTypes.GET_MENU,
      t: res.data.content,
      id: res.data._id,
    });
  };
}

export function updateMenu(type: any) {
  return async (dispatch: DispatchType, getState: any) => {
    const state = getState();
    updateM(type, state.type.id);
    return dispatch({
      type: actionTypes.UPDATE_MENU,
      t: type,
    });
  };
}
