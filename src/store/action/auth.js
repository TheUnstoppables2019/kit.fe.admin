import axios from 'axios';
import decode from 'jwt-decode';
import * as actionTypes from './actionTypes';
import configuration from '../../config/apis';

export const authStart = () => ({
    type: actionTypes.AUTH_START,
});

export const authSuccess = (token, userId, displayName, userImage) => ({
    type: actionTypes.AUTH_SUCCESS,
    idToken: token,
    userId,
    displayName,
    userImage,
});

export const authFail = (error) => ({
    type: actionTypes.AUTH_FAIL,
    error,
});
export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    localStorage.removeItem('userImage');
    localStorage.removeItem('userName');
    localStorage.removeItem('userImage');
    return {
        type: actionTypes.AUTH_LOGOUT,
    };
};

export const checkAuthTimeout = (expirationTime) => (dispatch) => {
    setTimeout(() => {
        dispatch(logout());
    }, expirationTime);
};

const auth = (email, password, url) => (dispatch) => {
    dispatch(authStart());
    const authData = {
        email,
        password,
        returnSecureToken: true,
    };
    axios
        .post(url, authData)
        .then((response) => {
            const {exp} = decode(response.data.token);
            const expirationDate = new Date();
            const ts = new Date().getTime();
            expirationDate.setTime(ts + exp / 10);
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('expirationDate', expirationDate);
            localStorage.setItem('userId', response.data.user.id);
            localStorage.setItem('userName', `${response.data.user.userName}`);
            localStorage.setItem('userImage', response.data.user.profileImg);
            dispatch(
                authSuccess(
                    response.data.token,
                    response.data.user.id,
                    `${response.data.user.userName}`,
                    response.data.user.profileImg
                ),
            );
        })
        .catch((err) => {
            dispatch(authFail(err));
        });
};

export const login = (email, password) => (dispatch) => {
    const loginUrl = `${configuration.api.kitmanapis}/api/v1/login`;
    dispatch(auth(email, password, loginUrl));
}

export const signUp = (email, password) => (dispatch) => {
    const signUpUrl = `${configuration.api.kitmanapis}/api/v1/sign-up`;
    dispatch(auth(email, password, signUpUrl));
}


export const authCheckState = () => (dispatch) => {
    const token = localStorage.getItem('token');
    if (!token) {
        dispatch(logout());
    } else {
        const expirationDate = new Date(localStorage.getItem('expirationDate'));
        if (expirationDate <= new Date()) {
            dispatch(logout());
        } else {
            const userId = localStorage.getItem('userId');
            dispatch(authSuccess(token, userId, localStorage.getItem('userName'), localStorage.getItem('userImage')));
            dispatch(checkAuthTimeout(expirationDate.getTime() - new Date().getTime()));
        }
    }
};
