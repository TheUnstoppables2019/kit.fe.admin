export const ADD_ARTICLE = 'ADD_ARTICLE';
export const GET_ARTICLE = 'GET_ARTICLE';
export const REMOVE_ARTICLE = 'REMOVE_ARTICLE';
export const GET_TYPE = 'GET_TYPE';
export const UPDATE_TYPE = 'UPDATE_TYPE';
export const ADD_USERFIELD = 'ADD_USERFIELD';
export const REMOVE_USERFIELD = 'REMOVE_USERFIELD';
export const UPDATE_USERFIELD = 'UPDATE_USERFIELD';

export const GET_MENU = 'GET_MENU';
export const UPDATE_MENU = 'UPDATE_MENU';

export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

export const SET_SUBMITTING = 'SET_SUBMITTING';
export const SET_HAS_SAVED = 'SET_HAS_SAVED';
