export interface IArticle {
    id: number
    title: string
    body: string
}

export type ArticleState = {
    articles: IArticle[]
}

export type getArticleAction = {
    type: string
}

export type ArticleAction = {
    type: string
}

export type DispatchType = (args: ArticleAction) => ArticleAction



