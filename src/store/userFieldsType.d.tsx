export type  UserFieldsState = {
    fields: any
}

export type UserFieldsAction = {
    type: string
    t: any
}

export type DispatchType = (args: UserFieldsAction) => UserFieldsAction
