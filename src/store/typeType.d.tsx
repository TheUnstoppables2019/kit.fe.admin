import React from 'react';
import {
  BarChart,
  CameraVideo,
  FileEarmark,
  FilePerson,
  Folder,
  X,
} from 'react-bootstrap-icons';
import Json from '../components/Template/Json/Json';
import CodeEditor from '../components/Template/CodeEditor/CodeEditor';
import Save from '../components/Template/Save/Save';
import ViewBlogs from '../components/Template/Blogs/ViewBlogs/ViewBlogs';
import VersionControl from '../components/Template/VersionControl/VersionControl';

export type Type = {
  name: string;
  url: string;
  icon: string;
  template: TYPE_TEMPLATE | TYPE_TEMPLATE[];
};

export type MenuState = {
  types: any;
  id: any;
};

export type MenuAction = {
  type: string;
  t: any;
  id?: any;
};

export type UIAction = {
  type: string;
  isSubmitting?: any;
  hasSaved?: any;
};

export enum TYPE_ICON {
  CAMERA_VIDEO,
  BAR_CHART,
  FOLDER,
  FILE_EAR_MARK,
  FILE_PERSON,
  CLOSE,
}

export const TYPE_ICON_COMPONENT = {
  [TYPE_ICON.CAMERA_VIDEO]: <CameraVideo />,
  [TYPE_ICON.BAR_CHART]: <BarChart />,
  [TYPE_ICON.FOLDER]: <Folder />,
  [TYPE_ICON.FILE_EAR_MARK]: <FileEarmark />,
  [TYPE_ICON.FILE_PERSON]: <FilePerson />,
  [TYPE_ICON.CLOSE]: <X />,
};

export enum TYPE_URL {
  ABOUT = '/about',
  SKILLS = '/skills',
  PROJECT = '/project',
  POSTS = '/post',
  RESUME = '/resume',
}

export enum TYPE_TEMPLATE {
  JSON = 'JSON',
  CODE_EDITOR = 'CODE_EDITOR',
  EMPTY = 'EMPTY',
  SAVE = 'SAVE',
  BLOG = 'BLOG',
  VERSION_CONTROL = 'VERSION_CONTROL',
}

export const TYPE_TEMPLATE_COMPONENT = {
  [TYPE_TEMPLATE.JSON]: <Json />,
  [TYPE_TEMPLATE.CODE_EDITOR]: <CodeEditor />,
  [TYPE_TEMPLATE.EMPTY]: null,
  [TYPE_TEMPLATE.SAVE]: <Save />,
  [TYPE_TEMPLATE.BLOG]: <ViewBlogs />,
  [TYPE_TEMPLATE.VERSION_CONTROL]: <VersionControl />,
};

export type DispatchType = (args: MenuAction) => MenuAction;
export type uiDispatchType = (args: UIAction) => UIAction;
