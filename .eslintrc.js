// .eslintrc.js
module.exports = {
  extends: ['airbnb-typescript-prettier'],
  rules: {
    'no-param-reassign': 0,
    '@typescript-eslint/no-use-before-define': 0,
    'no-console': 0,
    'react-hooks/exhaustive-deps' : 'off',
    'react/require-default-props' : 'off',
    '@typescript-eslint/interface-name-prefix' : 'off',
    'react/jsx-props-no-spreading' : 'off',
    'jsx-a11y/click-events-have-key-events': 'off',
    'jsx-a11y/no-noninteractive-element-interactions': 'off',
    'import/no-named-as-default' : 'off',
    'no-case-declarations' : 'off',
    'import/prefer-default-export' :'off'
  },
};
